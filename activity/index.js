/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function getUserInfo(){
		let userFullName = prompt('Please Enter your Full Name');
		let userAge = prompt('Please Enter your Age');
		let userLocation = prompt('Please Enter you Location');

		console.log('Hi ' + userFullName);
		console.log(userFullName + '\'s' + ' age is ' + userAge);
		console.log(userFullName + ' is located at ' + userLocation);
	};
	getUserInfo();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function displayUserFavoriteArtist(){
		let userFavorite = ['Back Street Boys', 'Olivia Rodrigo', 'Linkin Park', 'Cueshe', 'December Avenue'];

		console.log(userFavorite);
	}
	displayUserFavoriteArtist();
/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function displayUserFavoriteMovies(){
		console.log('1. The Avengers: Endgame');
		console.log('Tomatometer for Endgame: 94%');
		console.log('2. The Avengers: Infinity War');
		console.log('Tomatometer for Infinity War: 85%');
		console.log('3. Avengers: Age of Ultron');
		console.log('Tomatometer for Age of Ultron: 76%');
		console.log('4. Marvels\'s The Avengers');
		console.log('Tomatometer for The Avengers: 91%');
		console.log('5. Spider-Man: No Way Home');
		console.log('Tomatometer for No Way Home: 93%');
	}
	displayUserFavoriteMovies();
/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

function printUserFriends(){

	alert("Hi! Please add the names of your friends.");
	
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printUserFriends();
